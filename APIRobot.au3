
post_authentication_test()

Func post_authentication_test()
    Local $oHttp   = Null, _
          $oComErr = Null

    Local $iHttpStatus = 0

    Local $sResponse = "", _
          $sPostData = ""


    ConsoleWrite(@CRLF & "Executing API" & @CRLF)

    ;Set COM error handler
    $oComErr = ObjEvent("AutoIT.Error", "com_error_handler")

    ;Create a HTTP COM object
    $oHttp = ObjCreate("winhttp.winhttprequest.5.1")
    If @error Then
        ConsoleWrite("Unable to create http request object." & @CRLF)
        Exit -1
    EndIf
    ConsoleWrite("WinHttpRequest object created." & @CRLF)

    With $oHttp
        ;Open POST request
		.Open("POST", "http://localhost:5000/api/auth/login?username=imenmasseoudi9@gmail.com&password=Os20212021&#x22;", False)

        ;Set request headers and options
        .SetRequestHeader("Content-Type", "application/json")

        ;Send request
		.Send()
        If @error Then
            ConsoleWrite(StringFormat("SEND ERROR: (0x%X) %s", $oComErr.Number, $oComErr.Description) & @CRLF)
            Return
        EndIf

        ;Get status code and response
        $iHttpStatus = .Status
        $sResponse   = .ResponseText

        ;If status code isn't okay
        If $iHttpStatus <> 200 Then
            ConsoleWrite("HTTP Status  : " & String($iHttpStatus) & @CRLF)
            ConsoleWrite("HTTP Response: " & @CRLF & $sResponse & @CRLF)
            Return
        EndIf
    EndWith

    ConsoleWrite("API Response:" & @CRLF & $sResponse & @CRLF)


EndFunc

Func com_error_handler($oError)
    #forceref $oError
    Return
EndFunc