#include <IE.au3>
$oIE = _IECreate("https://www.youtube.com/")

; Wait for a browser page load to complete
_IELoadWait($oIE)
Sleep(7000)

; Attach to a browser control embedded in another window
Local $oIE = _IEAttach("YouTube", "embedded")
ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $oIE = ' & $oIE & @CRLF & '>Error code: ' & @error & '    Extended code: 0x' & Hex(@extended) & @CRLF) ;### Debug Console


; Store the field names where the important data will be sent
Local $nWorkOrderB   = _IEGetObjById($oIE, "endpoint")

;_IEImgClick($oIE, "toolactions_INSERT-tbb_image", "id")

Sleep(1000)
_IEAction($nWorkOrderB, "focus")
_IEAction($nWorkOrderB, "click")