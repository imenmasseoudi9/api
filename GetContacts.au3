#include <WinHttp.au3>
GetContacts()

Func GetContacts()
    Local $oHttp   = Null, _
          $oComErr = Null

    Local $iHttpStatus = 0

    Local $sResponse = "", _
          $sPostData = ""


    ConsoleWrite(@CRLF & "Executing API" & @CRLF)

    ;Set COM error handler
    $oComErr = ObjEvent("AutoIT.Error", "com_error_handler")

    ;Create a HTTP COM object
    $oHttp = ObjCreate("winhttp.winhttprequest.5.1")
    If @error Then
        ConsoleWrite("Unable to create http request object." & @CRLF)
        Exit -1
    EndIf
    ConsoleWrite("WinHttpRequest object created." & @CRLF)

    With $oHttp
        ;Open POST request
		.Open("Get", "http://localhost:5000/api/Contacts", False)

        ;Set request headers and options
        .SetRequestHeader("Content-Type", "application/json")
        .SetRequestHeader('Authorization','Basic ' & "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiaW1lbm1hc3Nlb3VkaTlAZ21haWwuY29tIiwic3ViIjoiaW1lbm1hc3Nlb3VkaTlAZ21haWwuY29tIiwianRpIjoiMTUzNDgwZWItZTZmMC00OWIyLTkyNWMtMmJhZjc5NWVjMjg4IiwiZXhwIjoxNjQwMzcwMzg3LCJpc3MiOiJhc3BuZXQtY29yZS1yZWFjdC10ZW1wbGF0ZSIsImF1ZCI6ImFzcG5ldC1jb3JlLXJlYWN0LXRlbXBsYXRlIn0.uD8ge5CuJ736HZReooCWdCOPjNqcMGbNiK9V5M98yfA")

        ;Send request
		.Send()
        If @error Then
            ConsoleWrite(StringFormat("SEND ERROR: (0x%X) %s", $oComErr.Number, $oComErr.Description) & @CRLF)
            Return
        EndIf

        ;Get status code and response
        $iHttpStatus = .Status
        $sResponse   = .ResponseText

        ;If status code isn't okay
        If $iHttpStatus <> 200 Then
            ConsoleWrite("HTTP Status  : " & String($iHttpStatus) & @CRLF)
            ConsoleWrite("HTTP Response: " & @CRLF & $sResponse & @CRLF)
            Return
        EndIf
    EndWith

    ConsoleWrite("API Response:" & @CRLF & $sResponse & @CRLF)
EndFunc

Func com_error_handler($oError)
    #forceref $oError
    Return
EndFunc